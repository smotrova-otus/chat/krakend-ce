package tokendecoder

import (
	"github.com/devopsfaith/krakend/config"
	"github.com/go-playground/validator/v10"
	"github.com/mitchellh/mapstructure"
)

const decoderConfigNamespace = "gitlab_com/smotrova-otus/chat/krakend-ce/middleware/tokendecoder"

type decoderConfig struct {
	UserIDKey  string `mapstructure:"user_id_key" validate:"required"`
	HeaderName string `mapstructure:"header_name" validate:"required"`
}

func parseDecoderConfig(config config.ExtraConfig) (cfg *decoderConfig, err error) {
	cfg = new(decoderConfig)

	t, ok := config[decoderConfigNamespace]
	if !ok {
		return nil, nil
	}

	err = mapstructure.Decode(t, cfg)
	if err != nil {
		return nil, err
	}

	validate := validator.New()
	err = validate.Struct(cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
