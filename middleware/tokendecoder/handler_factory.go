package tokendecoder

import (
	"fmt"
	"github.com/devopsfaith/krakend/config"
	"github.com/devopsfaith/krakend/proxy"
	krakendgin "github.com/devopsfaith/krakend/router/gin"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func NewHandlerFactory(nextHandlerFactory krakendgin.HandlerFactory, decoder *TokenDecoder) krakendgin.HandlerFactory {
	return func(config *config.EndpointConfig, proxy proxy.Proxy) gin.HandlerFunc {
		nextHandler := nextHandlerFactory(config, proxy)
		logger := decoder.logger

		if decoder.config == nil {
			logger.Debug(fmt.Sprintf(
				"token-decoder disabled for endpoint `%s` because service extra config doesn't defines token-decoder config",
				config.Endpoint,
			))
			return nextHandler
		}

		endpointConfig, err := parseEndpointConfig(config.ExtraConfig)
		if err != nil {
			logger.Error(fmt.Errorf(
				"error to enable token-decoder for endpoint `%s`: %v",
				config.Endpoint,
				err,
			))
			return nextHandler
		}
		if endpointConfig == nil {
			logger.Debug(fmt.Sprintf(
				"token-decoder disabled for endpoint `%s` because endpoint extra config doesn't defines token-decoder middleware",
				config.Endpoint,
			))
			return nextHandler
		}

		logger.Info(fmt.Sprintf("enable token-decoder for endpoint `%s`", config.Endpoint))

		handler := &handler{
			EndpointConfig: *endpointConfig,
			DecoderConfig:  *decoder.config,
			Parser:         jwt.Parser{},
			NextHandler:    nextHandler,
		}

		return handler.GinHandler
	}
}
