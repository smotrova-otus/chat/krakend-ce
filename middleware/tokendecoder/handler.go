package tokendecoder

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type handler struct {
	EndpointConfig endpointConfig
	DecoderConfig  decoderConfig
	Parser         jwt.Parser
	NextHandler    gin.HandlerFunc
}

func (h handler) GinHandler(ctx *gin.Context) {
	tokenString := ctx.GetHeader("Authorization")
	tokenString = strings.TrimPrefix(tokenString, "Bearer ")
	tokenString = strings.TrimPrefix(tokenString, "bearer ")

	if tokenString == "" {
		_ = ctx.AbortWithError(http.StatusBadRequest, fmt.Errorf("invalid auth token: %s", tokenString))
		return
	}

	claims := newClaims(claimsOptions{
		UserIDKey: h.DecoderConfig.UserIDKey,
	})

	_, _, err := h.Parser.ParseUnverified(tokenString, claims)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusBadRequest, fmt.Errorf("error parse token: %v", err))
		return
	}

	ctx.Request.Header.Add(h.DecoderConfig.HeaderName, fmt.Sprintf("%v", claims.UserID))

	h.NextHandler(ctx)
}
