package tokendecoder

import (
	"github.com/devopsfaith/krakend/config"
	"github.com/go-playground/validator/v10"
	"github.com/mitchellh/mapstructure"
)

const endpointConfigNamespace = "gitlab.com/smotrova-otus/chat/krakend-ce/middleware/tokendecoder"

type endpointConfig struct {
	// Future design to decode endpoint extra config fields.
}

func parseEndpointConfig(config config.ExtraConfig) (*endpointConfig, error) {
	t, ok := config[endpointConfigNamespace]
	if !ok {
		return nil, nil
	}

	cfg := &endpointConfig{}

	err := mapstructure.Decode(t, cfg)
	if err != nil {
		return nil, err
	}

	validate := validator.New()
	err = validate.Struct(cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
