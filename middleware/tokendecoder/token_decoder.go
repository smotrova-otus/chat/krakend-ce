package tokendecoder

import (
	"fmt"
	"github.com/devopsfaith/krakend/config"
	"github.com/devopsfaith/krakend/logging"
)

type TokenDecoder struct {
	config *decoderConfig
	logger logging.Logger
}

func NewTokenDecoder(logger logging.Logger, config config.ServiceConfig) (*TokenDecoder, error) {
	cfg, err := parseDecoderConfig(config.ExtraConfig)
	if err != nil {
		return nil, fmt.Errorf("error parse config: %v", err)
	}

	if cfg == nil {
		logger.Debug("token-decoder is disabled because service extra config doesn't defines token-decoder config")
	}

	return &TokenDecoder{
		config: cfg,
		logger: logger,
	}, nil
}
