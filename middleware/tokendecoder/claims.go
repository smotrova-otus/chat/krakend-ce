package tokendecoder

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
)

type claims struct {
	UserID  interface{}   `json:"-"`
	Options claimsOptions `json:"-"`
	jwt.StandardClaims
}

func (c *claims) UnmarshalJSON(raw []byte) error {
	err := json.Unmarshal(raw, &c.StandardClaims)
	if err != nil {
		return err
	}

	claimsMap := make(map[string]interface{})

	err = json.Unmarshal(raw, &claimsMap)
	if err != nil {
		return err
	}

	userID, ok := claimsMap[c.Options.UserIDKey]
	if !ok {
		return fmt.Errorf("can't find userID key in token")
	}

	c.UserID = userID

	return nil
}

type claimsOptions struct {
	UserIDKey string
}

func newClaims(options claimsOptions) *claims {
	return &claims{
		Options: options,
	}
}
